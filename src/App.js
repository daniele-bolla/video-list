import React, { Component } from 'react';
import './App.css';
import CreateVideoView from './components/createVideoView';
import ListVideos from './components/listVideos';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
					<h1>Video List</h1>
        </header>
				<main>
					<CreateVideoView></CreateVideoView>
					<ListVideos></ListVideos>
				</main>
      </div>
    );
  }
}

export default App;
