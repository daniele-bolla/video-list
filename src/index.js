import React from 'react';
import ReactDOM from 'react-dom';
import videosReducer from './reducers/videosReducers';
import { combineReducers, createStore } from 'redux'
import './index.css';
import * as serviceWorker from './serviceWorker';
import App from './App';
import { Provider } from 'react-redux';

const reducers = combineReducers({
	videos: videosReducer,
})
const store = createStore(reducers, {
	videos:{
		byId: ['#video_1'],
		byHash: {'#video_1':{
			id:'#video_1',
			title:'video',
			url:'http://media.w3.org/2010/05/sintel/trailer.mp4',
			duration:0
		}}
	},
},
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
      <Provider store={store}><App /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
